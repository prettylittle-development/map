// import the required modules
import {ElementData} from '../dom/ElementData';

/**
 * [map description]
 * @type {[type]}
 */
let map =
{
	/**
	 * initialize the map
	 * @param  {HTMLElement} root	Parent element to initialize all the toggle in
	 * @return null
	 */
	init : function(root = document)
	{
		// parameters used within this init function, these relate to html/css/js names
		let params =
		{
			data_name			: 'map'
		};

		// get all of the items in the page
		let maps				= root.querySelectorAll('[data-' + params.data_name + ']');

		// loop through them
		[].forEach.call(maps, function (map)
		{
			// default settings for the toggle
			let defaults =
			{
				height					: 300,
				options : {
					zoom				: 14,
					mapTypeId			: "roadmap",
					panControl			: true,
					zoomControl			: true,
					mapTypeControl		: false,
					scaleControl		: true,
					streetViewControl	: false,
					overviewMapControl	: true,
					scrollwheel			: false
				},
				lat						: false,
				lng						: false,
				// private
				_init					: false
			};

			// the the json embedded data
			let options			= ElementData.get(params.data_name, map, defaults);

			// if the toggle has already been initialized... bail out and don't reinit
			if(options._init)
			{
				return;
			}

			options._init		= true;

			// get any markers for the map, we do this before the map gets built as the innerHTML gets overwrittem
			let _markers		= map.querySelectorAll('[data-marker]');
			let markers			= [];

			[].forEach.call(_markers, function (marker)
			{
				markers.push
				(
					{
						options	: ElementData.get('marker', marker, {lat: 50, lng: 50, title: "undefined"}),
						content	: marker.innerHTML
					}
				);
			});

			options.options.center	= new google.maps.LatLng(options.lat, options.lng);

			// create the map
			let _map			= new google.maps.Map(map, options.options);

			// create a bounding area, so we can fit all of the markers on to the screen
			let boundaries		= new google.maps.LatLngBounds();

			// add the markers to the map
			[].forEach.call(markers, function (data)
			{
				let position	= new google.maps.LatLng(data.options.lat, data.options.lng);
				let marker		= new google.maps.Marker
									(
										{
											position	: position,
											title		: data.options.title,
											map			: _map,
											animation	: google.maps.Animation.DROP
										}
									);

				// create the infowindow
				let infowindow	= new google.maps.InfoWindow
				(
					{
						content	: data.content
					}
				);

				google.maps.event.addListener(infowindow, 'closeclick', function()
				{
					if(marker.getAnimation() !== null)
					{
						marker.setAnimation(null);
					}
				});

				// add a marker
				marker.addListener('click', function()
				{
					if(data.content)
					{
						// open the infowindow
						infowindow.open(_map, marker);
					}

					if(marker.getAnimation() !== null)
					{
						marker.setAnimation(null);
					}
					else
					{
						marker.setAnimation(google.maps.Animation.BOUNCE);
					}
				});

				// add the marker position to the boundaries
				boundaries.extend(position);
			});

			// if there are more than 1 markers on the map... fit them all on the map
			if(markers.length > 1)
			{
				_map.fitBounds(boundaries);
			}

			ElementData.set(params.data_name, map, options);
		});
	}
};

export default map;
